//
//  detailsViewController.swift
//  Integration_Task1
//
//  Created by Sierra 4 on 27/02/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit
import Kingfisher
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
class detailsViewController: UIViewController {
   
   // var passedImages:[String:AnyObject]?
    var username: String?
    var firstNme: String?
    var lastNme: String?
    var email: String?
    var idPassed: String?
    var url:String?
    var dictionary:[String:AnyObject]?
    var  gnderPassed: String?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailOutlet: UILabel!
    @IBOutlet weak var imageOutlet: UIImageView!
    @IBOutlet weak var lastNmeOutlet: UILabel!
    @IBOutlet weak var firstNmeOutlet: UILabel!
    @IBOutlet weak var ID: UILabel!
   
    @IBOutlet weak var gndrOutlet: UILabel!
   
    override func viewDidLoad() {
        super.viewDidLoad()
     firstNmeOutlet.text = firstNme
     lastNmeOutlet.text = lastNme
     nameLabel.text = username
   emailOutlet.text = email
            ID.text = idPassed
        gndrOutlet.text = gnderPassed
        
        if let picture = dictionary?["picture"] as? Dictionary<String,AnyObject>
        {
            if let data = picture["data"] as? Dictionary<String, AnyObject>
            {
                if let url = data["url"] as? String
                {
                    print(url)
                    imageOutlet.kf.setImage(with: URL(string:url))
                    
                }
            }
        }
        
    }
    
    
    @IBAction func logoutActionButton(_ sender: Any)
    {
       
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
        self.dismiss(animated: true, completion: nil)
           }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    

    

}
