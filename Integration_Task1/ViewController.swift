//
//  ViewController.swift
//  Integration_Task1
//
//  Created by Sierra 4 on 27/02/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import Google
import GoogleSignIn
import SVProgressHUD
import NVActivityIndicatorView

class ViewController: UIViewController,GIDSignInUIDelegate,GIDSignInDelegate {
    
    @IBOutlet weak var logOutBtnO: UIButton!
    var dic : [String : AnyObject]!
    var namePass :Any?
    var EmailPass :Any?
    @IBOutlet weak var btnfbLogin: UIButton!
    var idPass :Any?
    var url:Any?
    var frstNmPass : Any?
    var lastNmPass : Any?
    var genderPass : Any?
    
    var dic2 : [String : AnyObject]!

    var userId:String?
    var idToken:Any?
    var fullName:Any?
    var email:String?
    var familyName:Any?
        override func viewDidLoad()
    {
        super.viewDidLoad()
      //  logOutBtnO.isHidden = true
        GIDSignIn.sharedInstance().clientID = "20550551469-grp6p7r291lirudlq8d5hvnib9s436rq.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().clientID = "20550551469-grp6p7r291lirudlq8d5hvnib9s436rq.apps.googleusercontent.com"
//        let customFbBtn = UIButton(type: .system)
//        customFbBtn.backgroundColor = .red
//        customFbBtn.setTitle("Facebook Login", for: .normal)
//        customFbBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
//        customFbBtn.setTitleColor(.white, for: .normal)
//        customFbBtn.frame = CGRect(x: 0, y: 0, width: 180, height: 40)
//        customFbBtn.center = view.center
//        view.addSubview(customFbBtn)
//        customFbBtn.addTarget(self, action: #selector(self.loginFacebookAction), for: .touchUpInside)
//        
//        //google button
//        let googleButton = GIDSignInButton()
//        googleButton.frame = CGRect(x: 16, y: 116 + 66, width: view.frame.width - 30, height: 50)
//        view.addSubview(googleButton)
//        GIDSignIn.sharedInstance().uiDelegate = self
//        GIDSignIn.sharedInstance().clientID = "20550551469-grp6p7r291lirudlq8d5hvnib9s436rq.apps.googleusercontent.com"
//        //GIDSignIn.sharedInstance().signOut()
//        googleButton.addTarget(self, action: #selector(signINB), for: .touchUpInside)
    }
    
    @IBAction func logOutButton(_ sender: Any) {
       
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
        GIDSignIn.sharedInstance().signOut()
        //logOutBtnO.isHidden = false
    }
    
   
    
    @IBAction func FacebookActionButton(_ sender: Any)
    {
       // self.loginFacebookAction()
        let loginManager = LoginManager()
        loginManager.logIn([.publicProfile,.email], viewController: self)
        { loginResult in
            switch loginResult
            {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success( _, _, _):
                self.FacebookUserData()
               
                self.btnfbLogin.isUserInteractionEnabled = false
                
                
                
            }
        }
    }
   
    
    @IBAction func googleActionButton(_ sender: Any) {
        
        GIDSignIn.sharedInstance().signIn()
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    
    
    func handleCustomGoogleLogin()
    {
          GIDSignIn.sharedInstance().signIn()
        logOutBtnO.isHidden = false
         GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    func loginFacebookAction(sender: AnyObject) {//action of the custom button in the storyboard
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.FacebookUserData()
                    
//                    SVProgressHUD.show()
//                    SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
//                    SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
                }
            }
        }
    }
    
    
    func FacebookUserData(){
        if((FBSDKAccessToken.current()) != nil)
        {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email, gender"]).start(completionHandler: { (connection, result, error) -> Void in
                
                if (error == nil)
                {
                    self.dic = result as! [String : AnyObject]
                    self.namePass = self.dic["name"]
                    self.EmailPass = self.dic["email"]
                    self.idPass = self.dic["id"]
                    self.frstNmPass = self.dic["first_name"]
                    self.lastNmPass = self.dic["last_name"]
                    self.genderPass = self.dic[" gender"]
                    let tempdata = self.dic["picture"]!
                    let userdata = tempdata["data"]!
                    self.performSegue(withIdentifier:"segue", sender: self)
                }
            })
            btnfbLogin.isUserInteractionEnabled = true
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if(segue.identifier=="segue")
        {
        if let destination = segue.destination as? detailsViewController
        {
            destination.username = namePass as! String?
            destination.firstNme = frstNmPass as! String?
            destination.lastNme = lastNmPass as! String?
            destination.gnderPassed = genderPass as! String?
            destination.email = EmailPass as! String?
            destination.idPassed  = idPass as! String?
            destination.dictionary = self.dic!
           // destination.url = url as! String?
            }
        }
    else if(segue.identifier=="googleSegue")
            {
            if let destinationA = segue.destination as? GoogleViewController
            {
            destinationA.UserIdpassed  = userId 
            destinationA.emailIdpassed  = email
            destinationA.namepassed  = fullName as! String?;
           // destinationA.dictionary1 = self.dic2!
        
                }
    }
    }
    
    func signInWillDispatch(signIn: GIDSignIn!, error: Error!) {
        //myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
        //print("Sign in presented")
    }
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
        // print("Sign in dismissed")
    }
   
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!)  {
            // Perform any operations on signed in user here.
        
            // userId = user.userID                  // For client-side use only!
            // idToken = user.authentication.idToken // Safe to send to the server
             fullName = user.profile.name
             //  givenName = user.profile.givenName
            familyName = user.profile.familyName
            email = user.profile.email
            if let err = error {

                         print("Failed to log into Google",err)
                return
            }
            print("Logged in successfully",user.profile.email)
            print("Logged in successfully",user.profile.givenName)
            print("Logged in successfully",user.profile.familyName)
            print("Logged in successfully",user.profile.name)
        self.performSegue(withIdentifier:"googleSegue", sender: self)
       
        }
    

}
